#!/usr/bin/env node
const { spawn } = require('child_process');
const path = require('path');
const fs = require('fs-extra');
const figlet = require('figlet');
const chalk = require('chalk');
const logSymbols = require('log-symbols');
const ora = require('ora');
const boxen = require('boxen');

const arguments = process.argv.slice(2);
const processStartTime = new Date().getTime();

if (arguments.length === 0) {
  console.log(logSymbols.error, chalk.red('Invalid args...'));
  console.log(chalk.blue('Quick start...'));
  console.group();
  console.log(chalk.green('exp .'), '(OR express-draft .)');
  console.log(`installs in current directory (${process.cwd()})`);
  console.log('OR');
  console.log(chalk.green('exp mApp'), '(OR express-draft mApp)');
  console.log(
    `installs in "mApp" directory (${path.join(process.cwd(), 'mApp')})`
  );
  console.groupEnd();
  return 0;
}

const rootDirectory = path.join(process.cwd(), arguments[0]);

function copyFiles() {
  return new Promise(async (resolve, reject) => {
    const spinner = ora('Pouring files ...').start()
    try {
      const sourceConfigApiFile = path.join(__dirname, '..', 'config', 'api.js')
      const sourceConfigDatabaseFile = path.join(__dirname, '..', 'config', 'database.js')
      const destinationConfigPath = path.join(rootDirectory, 'config')
      await fs.mkdir(destinationConfigPath)
      const destinationConfigApiFile = path.join(destinationConfigPath, 'api.js')
      const destinationConfigDatabaseFile = path.join(destinationConfigPath, 'database.js')
      await fs.copyFile(sourceConfigApiFile, destinationConfigDatabaseFile)
      await fs.copyFile(sourceConfigDatabaseFile, destinationConfigApiFile)

      const destinationDatabaseScriptPath = path.join(rootDirectory, 'database')
      await fs.mkdir(destinationDatabaseScriptPath)

      const destinationSrcPath = path.join(rootDirectory, 'src')
      await fs.mkdir(destinationSrcPath)

      const destinationApiPath = path.join(destinationSrcPath, 'api')
      await fs.mkdir(destinationApiPath)
      const destinationApiComponentPath = path.join(destinationApiPath, 'user')
      await fs.mkdir(destinationApiComponentPath)
      const destinationApiComponentControllerPath = path.join(destinationApiComponentPath, 'controllers')
      await fs.mkdir(destinationApiComponentControllerPath)
      const destinationApiComponentModelPath = path.join(destinationApiComponentPath, 'models')
      await fs.mkdir(destinationApiComponentModelPath)
      const destinationApiComponentRoutesPath = path.join(destinationApiComponentPath, 'routes')
      await fs.mkdir(destinationApiComponentRoutesPath)
      const destinationApiComponentTestsPath = path.join(destinationApiComponentPath, 'tests')
      await fs.mkdir(destinationApiComponentTestsPath)

      const destinationMiddlewarePath = path.join(destinationSrcPath, 'middleware')
      await fs.mkdir(destinationMiddlewarePath)
      const destinationUtilsPath = path.join(destinationSrcPath, 'utils')
      await fs.mkdir(destinationUtilsPath)

      const sourceAppJSFile = path.join(__dirname, '..', 'src', 'app.js')
      const destinationAppJSFile = path.join(destinationSrcPath, 'app.js')
      await fs.copyFile(sourceAppJSFile, destinationAppJSFile)

      const sourceServerJSFile = path.join(__dirname, '..', 'src', 'server.js')
      const destinationServerJSFile = path.join(destinationSrcPath, 'server.js')
      await fs.copyFile(sourceServerJSFile, destinationServerJSFile)

      const sourceEnvFile = path.join(__dirname, '..',  'default.env')
      const destinationEnvFile = path.join(rootDirectory, '.env.dev')
      await fs.copyFile(sourceEnvFile, destinationEnvFile)

      const sourceEsLintFile = path.join(__dirname, '..',  'default.eslint')
      const destinationEsLintFile = path.join(rootDirectory, '.eslintrc.json')
      await fs.copyFile(sourceEsLintFile, destinationEsLintFile)

      const sourcePrettierFile = path.join(__dirname, '..',  'default.prettier')
      const destinationPrettierFile = path.join(rootDirectory, '.prettierrc')
      await fs.copyFile(sourcePrettierFile, destinationPrettierFile)

      spinner.succeed()
      resolve()
    } catch (error) {
      spinner.fail()
      reject(error)
    }
  })
}

function modifyPackageJson() {
  return new Promise(async (resolve, reject) => {
    const spinner = ora('Creating scripts ...').start()
    try {
      const sourcePackageFile = path.join(rootDirectory, 'package.json')
      const packageFileContent = await fs.readFile(sourcePackageFile, { encoding: 'utf-8' })
      let packageJson = JSON.parse(packageFileContent)
      // let packageJson = await fs.readJson(pkgSrc);

      packageJson = {
        ...packageJson,
        main: 'server.js',
        scripts: {
          start: 'node ./src/server.js',
          dev: 'nodemon ./src/server.js',
        }
      }
      const destinationPackageFile = path.join(rootDirectory, 'package.json')
      await fs.writeFile(destinationPackageFile, JSON.stringify(packageJson, null, 2))
      // await fs.writeJSON(pkgDest, packageJson);
      spinner.succeed()
      resolve()
    } catch (error) {
      spinner.fail()
      reject(error)
    }
  })
}

function installScript(command, args, spinnerText) {
  return new Promise((resolve, reject) => {
    const spinner = ora({ text: spinnerText, spinner: 'dots' }).start()
    const child = spawn(command, args, { cwd: rootDirectory, shell: true })
    // child.stderr.on('data', (data) => console.log('stderr...'));
    // child.stdout.on('data', (data) => console.log('stdout...'));
    child.on('exit', (code, signal) => {
      if (code) {
        spinner.fail()
        console.log(`Process exit with code: ${code}`)
        // eslint-disable-next-line prefer-promise-reject-errors
        reject(`Process exit with code: ${code}`)
      } else if (signal) {
        spinner.fail()
        console.log(`Process exit with signal: ${signal}`)
        // eslint-disable-next-line prefer-promise-reject-errors
        reject(`Process exit with signal: ${signal}`)
      } else {
        spinner.succeed()
        resolve()
      }
    })
  })
}
function done() {
  console.log(chalk.yellow('------------------------------------'))
  console.log('Begin by typing:')
  console.group()
  console.log(chalk.blue('cd'), arguments[0])
  console.log(chalk.blue('npm run dev'))
  console.group()
  console.log('starts the development server (using nodemon 🧐)')
  console.groupEnd()
  console.log(chalk.blue('npm start'))
  console.group()
  console.log(`starts the server (using node 😁)`)
  console.groupEnd()
  console.groupEnd()
  console.log(chalk.yellow('------------------------------------'))

  const endTime = new Date().getTime()
  const timeDifference = (endTime - processStartTime) / 1000
  console.log(`✅ Done in ${timeDifference} seconds ✨`)
  console.log('🌈 Happy hacking 🦄')
}
async function createApp() {
  try {
    if (!(await fs.pathExists(rootDirectory))) {
      await fs.mkdir(rootDirectory);
    }

    const files = await fs.readdir(rootDirectory);
    if (files.length > 0) {
      console.log(logSymbols.error,
        `Path ${chalk.green(rootDirectory)} is not empty, ${chalk.red(
          'aborting'
        )}`
      );
      return;
    }
    console.log('🚚 Bootstrapping Express app in', chalk.green(rootDirectory), '\n');

    await installScript('npm', ['init', '-y'], 'Creating Package.json ...');
    await installScript(
      'npm',
      ['i', 'express', 'dotenv', 'mongoose', 'morgan', 'joi'],
      'Installing dependencies ...'
    );
    await installScript(
      'npm',
      [
        'i',
        '-D',
        'nodemon',
        'eslint',
        'eslint-config-airbnb',
        'eslint-config-prettier',
        'eslint-plugin-import',
        'eslint-plugin-jsx-a11y',
        'eslint-plugin-node',
        'eslint-plugin-prettier',
        'eslint-plugin-react',
        'jest',
        'prettier',
      ],
      'Installing dev dependencies ...'
    );
    await copyFiles();
    await modifyPackageJson();
    done();
  } catch (error) {
    console.log(error);
  }
}

figlet('Exp', { font: 'Ghost' }, (err, data) => {
  if (err) {
    console.log('Something went wrong...');
    console.log(err);
    return;
  }
  console.log(chalk.green(data));
  createApp();
});
