const dotenv = require('dotenv');

dotenv.config({ path: './.env.dev' });
const dataBaseConnection = require('../config/database');

process.on('uncaughtException', (error) => {
  console.log('UncaughtException Shuting down ...');
  console.log(error);
  process.exit(1);
});

const app = require('./app');

dataBaseConnection.mongoose();

let PORT = process.env.PORT;
if (PORT === null || PORT === '') {
  PORT = 5000;
}

const server = app.listen(PORT, () => {
  console.log(`Server Started on Port ${PORT}...`);
});

process.on('unhandledRejection', (error) => {
  console.log('UnhandledRejection Shuting down ...');
  console.log(error);
  server.close(() => {
    process.exit(1);
  });
});
