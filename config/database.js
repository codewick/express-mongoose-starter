const mongoose = require('mongoose');

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

const connectMongoose = () => {
  mongoose
    .connect(`${DB}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then((connection) => {
      console.log('DB connection Successful');
    })
    .catch((error) => {
      console.log(error.name, error.message);
    });
};

const dataBaseConnection = {
  mongoose: connectMongoose,
};

module.exports = dataBaseConnection;
